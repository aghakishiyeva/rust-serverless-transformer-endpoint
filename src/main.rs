use crate::analyze_sentiment;

mod analyze_sentiment;

fn main() {
    match analyze_sentiment("I love this library!") {
        Ok((sentiment, confidence)) => {
            println!("Sentiment: {}", sentiment);
            println!("Confidence: {}", confidence);
        }
        Err(e) => e.print_and_set_sys_last_vars(),
    }
}
