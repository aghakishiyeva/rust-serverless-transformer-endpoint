use pyo3::prelude::*;
use pyo3::types::IntoPyDict;

pub fn analyze_sentiment(text: &str) -> PyResult<(String, f64)> {
    // Initialize the Python interpreter
    let gil = Python::acquire_gil();
    let py = gil.python();

    // Import the Python module
    let sentiment_module = PyModule::new(py, "sentiment_analysis")?;
    sentiment_module.add("analyze_sentiment", |py, text: &str| {
        let locals = [("text", text)].into_py_dict(py);
        let result = py.run(
            "
            from sentiment_analysis import analyze_sentiment
            analyze_sentiment(text)
            ",
            None,
            Some(&locals),
        )?;
        Ok(result.extract::<(String, f64)>()?)
    })?;

    // Call the Python function
    sentiment_module.call("analyze_sentiment", (text,))
}
