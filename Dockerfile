# Use the official Rust image from Docker Hub
FROM rust:latest

# Set the working directory inside the container
WORKDIR /usr/src/app

# Copy the Rust project files into the container
COPY . .

# Install Python and pip (for Python dependencies)
RUN apt-get update && apt-get install -y python3 python3-pip

# Install Python dependencies
RUN pip3 install --no-cache-dir transformers

# Build the Rust project
RUN cargo build --release

# Set the entry point to run the compiled Rust executable
CMD ["./target/release/rust_python_wrapper"]
