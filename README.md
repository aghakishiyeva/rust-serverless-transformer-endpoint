# Rust Serverless Transformer Endpoint

This project aims to deploy a Hugging Face Rust transformer as a serverless application on AWS Lambda. The transformer will analyze the sentiment of text using a pre-trained model.

## Requirements

To successfully set up the project, you need to fulfill the following requirements:

1. **Dockerize Hugging Face Rust Transformer:** Containerize the Rust transformer using Docker.

2. **Deploy Container to AWS Lambda:** Deploy the Docker container to AWS Lambda as a serverless function.

3. **Implement Query Endpoint:** Create an API endpoint that triggers the Lambda function and returns the sentiment analysis result.

## How to Dockerize, Deploy, and Implement

Follow these steps to Dockerize the Hugging Face Rust transformer, deploy the container to AWS Lambda, and implement a query endpoint:

### 1. Dockerize Hugging Face Rust Transformer

1. Ensure Docker is installed on your local machine.

2. Clone this repository:

`git clone <https://gitlab.com/aghakishiyeva/rust-serverless-transformer-endpoint>`


3. Navigate to the project directory:

`cd rust-serverless-transformer-endpoint`


4. Build the Docker image:

`docker build -t hugging-face-rust-transformer .`

### 2. Deploy Container to AWS Lambda

1. Push the Docker image to a container registry like Amazon ECR.

2. Use the AWS CLI or AWS Management Console to create a Lambda function:

```aws lambda create-function --function-name hugging-face-transformer --package-type Image --code ImageUri=<your-container-uri> --role <your-role-arn> --memory-size 512 --timeout 30```

### 3. Implement Query Endpoint

1. Create an API Gateway endpoint with a suitable HTTP method (e.g., POST).

2. Configure the integration to trigger the Lambda function created earlier.

3. Test the endpoint by sending requests with text data to perform sentiment analysis.

## Conclusion

By following these steps, you can successfully Dockerize the Hugging Face Rust transformer, deploy it to AWS Lambda, and implement a query endpoint for sentiment analysis.


